import './Styles/App.css'
import React from 'react';
import Nav from './Components/Nav';
import Form from './Components/Form';
import Items from './Components/Items';
import axios from 'axios';


// Main components
class App extends React.Component {

  state = {
    data:[],
    loading:false,
  };

searchPostal = async code => {
  this.setState({
    loading:true,
  });

  if(code ==="") {
    console.log("Waiting");
  } else {
    const resp = await axios.get(`https://aqueous-sea-55247.herokuapp.com/find/${code}/`);
    console.log(resp.data);
    this.setState({
      data:resp.data,
    })
  }
}

  // Search using postal codes in Django API
  // searchPostal = async postal => {
    
  //   if (postal === "") {
  //     console.log(`waiting....`);
  //   } else {
  //     fetch(`http://127.0.0.1:8000/find/${postal}/`, {
  //       method:'GET',
  //       headers: {
  //         'Content-Type':'application/json',
  //       },
  //       mode:'cors',
  //     }).then(response => response.json())
  //     .then(data => {
  //       this.setState({
  //         info: data,
  //         loading: false,
  //       })
  //     }).catch((error) => {
  //       console.log(error);
  //     })
  //   }
  // }

  render() {
    return (
      <div>
        <Nav title={'Postal Code'} icon={'fab fa-react'}/>
        <div className={'container'}>
          <Form SearchPostal={this.searchPostal} />
          <Items loading={this.state.loading} postal={this.state.data} />
        </div> 
      </div>
    );
  }
}

export default App;
