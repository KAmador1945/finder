import React from 'react';



const ItemsInfo = props => {
    const {munip, depart, nombre_vecd} = props.data;
    const {nombre_mn, postal} = props.data;

    if (nombre_mn !==""){
        return(
            <ul className={'list-group'}>
                <li>{depart ? `Departamento Id => ${depart}`:`Municipio id ==>${munip}`}</li>
                <li>{nombre_mn ? ` Municipio => ${nombre_mn}`:`Vecindario ==> ${nombre_vecd}`}</li>
                <li>Codigo Postal => {postal ? `${postal}`:'Sin Relacionar'}</li>
            </ul>
        )
    } else {
        return(
            <ul className={'list-group'}>
                <li className={'list-group-item'}>{munip}</li>
            </ul>
        )
    }
}

export default ItemsInfo;