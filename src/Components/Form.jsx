import React from 'react';


class Form extends React.Component {

    state = {
        postal:'',
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.SearchPostal(this.state.postal);
        this.setState({
            postal:'',
        })
    }

    onChange = (e) => {
        e.preventDefault();
        this.setState({
            [e.target.name]:e.target.value,
        })
    }

    render() {
        return(
            <div className={'row mt-3'}>
                <div className={'col-md-12'}>
                    <form className={'form'} onSubmit={this.onSubmit.bind(this)}>
                        <div className={'input-group'}>
                            <input type="text" name={'postal'} onChange={this.onChange} className={'form-control'} placeholder={'Type a postal code'}/>
                            <span className={'input-group-btn'}>
                                <input type="submit" value={'Search'} className={'btn btn-primary'}/>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Form;