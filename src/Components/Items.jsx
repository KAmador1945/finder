import React from 'react';
import ItemsInfo from './ItemsInfo';


class Items extends React.Component {
    render() {
        return(
           <div style={itemsStyle}>
               {
                    this.props.postal.map((data) => (
                        <ItemsInfo key={data.id}  data={data}/>
                    ))
                }
           </div>
        )
    }
}


const itemsStyle = {
    marginTop:'5%',
}

export default Items;