import React from 'react'


const Nav = (props) => {
    return(
        <nav className={'navbar bg-primary'}>
            <div className={'container-fluid'}>
                <a href='/' className={'navbar-brand'}>
                    <i className={props.icon}></i> {props.title}
                </a>
            </div>
        </nav>
    )
};

export default Nav;